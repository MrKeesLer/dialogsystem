package fr.mrkeesler.dialog;

import fr.mrkeesler.base.Line;

public class DialogLine extends Line {

    private String code;
    private String message;
    private String nextCode;

    public DialogLine(String c,String m,String n){
        this.code = c;
        this.message = m;
        this.nextCode = n;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNextCode() {
        return nextCode;
    }

    public void setNextCode(String nextCode) {
        this.nextCode = nextCode;
    }

}
