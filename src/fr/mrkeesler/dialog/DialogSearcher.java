package fr.mrkeesler.dialog;

import java.util.List;

public class DialogSearcher {

    /**
     * @param code is the code you search
     * @param lines is all the lines
     * @return the dialog line if it find it and null is the code doesn't exist
     */
    public static DialogLine getLine(String code, List<DialogLine> lines){
        for(DialogLine dl:lines){
            if(dl.getCode().equals(code)) return dl;
        }
        return null;
    }
}
