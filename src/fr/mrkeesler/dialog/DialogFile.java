package fr.mrkeesler.dialog;

import com.opencsv.CSVReader;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DialogFile {

    private String firstCode;
    private String fileName;
    private List<DialogLine> lines;

    /**
     * Construct a DialogFile
     * @param fileName is the name of the file ( whitout the .csv )
     */
    public DialogFile(String fileName) {
        this.fileName = fileName;
        try {
            Reader reader = Files.newBufferedReader(Paths.get("res/dialog/" + fileName + ".csv"));
            CSVReader csvReader = new CSVReader(reader, ';');
            this.lines = DialogLineGenerator.generateAllLine(csvReader.readAll());
            firstCode = this.lines.get(0).getCode();
        } catch (Exception e) {
            System.out.println("Something bad occured");
        }
    }

    public String getFirstCode() {
        return firstCode;
    }

    public String getFileName() {
        return fileName;
    }

    public List<DialogLine> getLines() {
        return lines;
    }
}
