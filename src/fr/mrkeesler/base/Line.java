package fr.mrkeesler.base;

public abstract class Line {

    public abstract String getCode();
    public abstract String getMessage();
    public abstract String getNextCode();
}
